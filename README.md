# Angular on S3 project sample

This project sample shows the usage of _to be continuous_ templates:

* Angular
* SonarQube (from [sonarcloud.io](https://sonarcloud.io/))
* S3 (from [Flexible Engine - Object Storage Service](https://cloud.orange-business.com/en/offers/infrastructure-iaas/public-cloud/features/object-storage-service/))

The Angular application is based on:

* Official [Tour of Heroes](https://angular.io/tutorial) tutorial,
* [e2e-testing-Angular-Protractor-Jasmine](https://github.com/dhormale/e2e-testing-Angular-Protractor-Jasmine) for tests implementation.

## Angular template features

This project uses the following features from the GitLab CI Angular template:

* Overrides the default Angular docker image by declaring the `$NG_CLI_IMAGE` variable in the `.gitlab-ci.yml` file,
* Enables [JUnit test report integration](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html) for `ng test` by enabling and configuring [karma-junit-reporter](https://www.npmjs.com/package/karma-junit-reporter) in [karma.conf.js](karma.conf.js#L36),
* Enables [code coverage integration](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html) for `ng test` by enabling and configuring [karma-coverage](https://www.npmjs.com/package/karma-coverage) in [karma.conf.js](karma.conf.js#L30),
* Enables `e2e` tests with `$NG_E2E_ENABLED`,
* Enables [JUnit test report integration](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html) for `ng e2e` by enabling and configuring `JUnitXmlReporter` from [jasmine-reporters](https://www.npmjs.com/package/jasmine-reporters) in [protractor.conf.js](e2e/protractor.conf.js#L32),
* Uses [scoped variables syntax](https://to-be-continuous.gitlab.io/doc/usage/#scoped-variables) to override default Angular build
options for `master` branch only.

## SonarQube template features

This project uses the following features from the SonarQube template:

* Defines the `SONAR_HOST_URL` (SonarQube server host),
* Defines `organization` & `projectKey` from [sonarcloud.io](https://sonarcloud.io/) in `sonar-project.properties`,
* Defines :lock: `$SONAR_TOKEN` as secret CI/CD variable,
* Uses the `sonar-project.properties` to specify project specific configuration:
    * source and test folders,
    * code coverage report,
    * linter report,
    * unit test reports.

## S3 template features

This project uses the following features from the GitLab CI S3 template:

* Defines mandatory `$S3_ENDPOINT_HOST`,
* Defines mandatory `$S3_ACCESS_KEY` and `$S3_SECRET_KEY` as secrets project variables,
* Set `$S3_DEPLOY_FILES` to define the Angular output build directory as the deployable S3 resources.

The GitLab CI S3 template also implements environments integration in GitLab:

* deployment environment integrated in merge requests,
* review environment cleanup support (manually or when the related development branch is deleted).
