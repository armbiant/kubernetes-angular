const baseUrl = process.env.environment_url || "http://localhost:4200";
module.exports = {
  ci: {
    collect: {
      settings: { chromeFlags: "--no-sandbox" },
      startServerCommand: "npm run start",
      url: [baseUrl],
    },
    assert: {
      preset: "lighthouse:no-pwa",
      assertions: {
        "categories:performance": "warn",
        "categories:accessibility": "warn",
        "bf-cache": "warn",
        "csp-xss": "warn",
        "installable-manifest": "warn",
        "meta-description": "off",
        "total-byte-weight": "warn",
        "unminified-javascript": "warn",
        "unused-javascript": "warn",
        "uses-text-compression": "warn",
        "categories:pwa": "off",
        "meta-viewport": "off",
        "uses-rel-preconnect": "off",
      },
    },
  },
};
