import { browser, by, element, ElementArrayFinder, ElementFinder } from 'protractor';

export class AppPage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl);
  }

  async getTitleText(): Promise<string> {
    return element(by.css('app-root h1')).getText();
  }

  getDashboardButton(): ElementFinder {
    return element.all(by.css('app-root nav a')).first();
  }

  getHeroesButton(): ElementFinder {
    return element.all(by.css('app-root nav a')).last();
  }

  getTopHeroesLinks(): ElementArrayFinder {
    return element.all(by.css('app-root app-dashboard .heroes-menu a'));
  }

  getSearchBox(): ElementFinder {
    return element(by.css('#search-box'));
  }

  getSearchResultItems(): ElementArrayFinder {
    return element.all(by.css('.search-result li'));
  }

  // =================================================================

  getSelectedHeroSubview(): ElementFinder {
    return element(by.css('app-root app-heroes > div:last-child'));
  }

  // =================================================================

  getAllHeroes(): ElementArrayFinder {
    return element.all(by.css('app-root app-heroes li'));
  }

  // async getHeroes(): Promise<ElementFinder> {
  //   return element(by.css('app-root app-heroes'));
  // }

  // =================================================================

  getHeroDetail(): ElementFinder {
    return element(by.css('app-root app-hero-detail'));
  }
}
